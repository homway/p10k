FROM ubuntu:20.04

ARG USERNAME=user1
ARG USER_UID=1000
ARG USER_GID=$USER_UID

ENV TERM=xterm-256color
ENV COLORTERM=truecolor

RUN groupadd --gid $USER_GID $USERNAME \
    && useradd -m -s /bin/zsh --uid $USER_UID --gid $USER_GID $USERNAME \
    && apt-get update \
    && apt-get install -y sudo git zsh curl wget nano vim \
    && echo $USERNAME ALL=\(root\) NOPASSWD:ALL > /etc/sudoers.d/$USERNAME \
    && chmod 0440 /etc/sudoers.d/$USERNAME \
    && apt-get autoremove -y \
    && apt-get clean -y \
    && rm -rf /var/lib/apt/lists/*
  
USER $USERNAME
WORKDIR /home/$USERNAME

# oh-my-zsh
RUN sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)" "" --unattended

# p10k
RUN git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/themes/powerlevel10k
RUN sed -i 's/ZSH_THEME="robbyrussell"/ZSH_THEME="powerlevel10k\/powerlevel10k"/g' .zshrc
COPY .p10k.zsh .p10k.zsh
RUN sed -i '1i if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then\n\tsource "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"\nfi' .zshrc
RUN sed -i '$i [[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh' .zshrc

# plugin
RUN git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions
RUN git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting
RUN git clone https://github.com/zsh-users/zsh-history-substring-search ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-history-substring-search
RUN sed -i 's/plugins=(git)/plugins=(git docker dotnet gcloud kubectl zsh-autosuggestions zsh-syntax-highlighting zsh-history-substring-search)/g' .zshrc
RUN sed -i '/source $ZSH\/oh-my-zsh.sh/a ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE="fg=6"\nbindkey "^[^[[A" history-substring-search-up\nbindkey "^[^[[B" history-substring-search-down' .zshrc

ENTRYPOINT ["zsh"]
CMD ["-l"]
