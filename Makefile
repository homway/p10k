.PHONY: build

build:
	docker build -t armin/p10k:0.0.1 .

armin:
	docker build --build-arg USERNAME=armin -t armin/p10k:armin-0.0.1 .

clean:
	docker rmi armin:0.1